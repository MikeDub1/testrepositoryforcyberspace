// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "PatcherDemo/PDGameInstance.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePDGameInstance() {}
// Cross Module References
	PATCHERDEMO_API UFunction* Z_Construct_UDelegateFunction_PatcherDemo_ChunkMountedDelegate__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_PatcherDemo();
	PATCHERDEMO_API UFunction* Z_Construct_UDelegateFunction_PatcherDemo_PatchCompleteDelegate__DelegateSignature();
	PATCHERDEMO_API UScriptStruct* Z_Construct_UScriptStruct_FPPatchStats();
	PATCHERDEMO_API UClass* Z_Construct_UClass_UPDGameInstance_NoRegister();
	PATCHERDEMO_API UClass* Z_Construct_UClass_UPDGameInstance();
	ENGINE_API UClass* Z_Construct_UClass_UGameInstance();
// End Cross Module References
	struct Z_Construct_UDelegateFunction_PatcherDemo_ChunkMountedDelegate__DelegateSignature_Statics
	{
		struct _Script_PatcherDemo_eventChunkMountedDelegate_Parms
		{
			int32 ChunkID;
			bool Succeeded;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ChunkID;
		static void NewProp_Succeeded_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_Succeeded;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UDelegateFunction_PatcherDemo_ChunkMountedDelegate__DelegateSignature_Statics::NewProp_ChunkID = { "ChunkID", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_PatcherDemo_eventChunkMountedDelegate_Parms, ChunkID), METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UDelegateFunction_PatcherDemo_ChunkMountedDelegate__DelegateSignature_Statics::NewProp_Succeeded_SetBit(void* Obj)
	{
		((_Script_PatcherDemo_eventChunkMountedDelegate_Parms*)Obj)->Succeeded = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UDelegateFunction_PatcherDemo_ChunkMountedDelegate__DelegateSignature_Statics::NewProp_Succeeded = { "Succeeded", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(_Script_PatcherDemo_eventChunkMountedDelegate_Parms), &Z_Construct_UDelegateFunction_PatcherDemo_ChunkMountedDelegate__DelegateSignature_Statics::NewProp_Succeeded_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_PatcherDemo_ChunkMountedDelegate__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_PatcherDemo_ChunkMountedDelegate__DelegateSignature_Statics::NewProp_ChunkID,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_PatcherDemo_ChunkMountedDelegate__DelegateSignature_Statics::NewProp_Succeeded,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_PatcherDemo_ChunkMountedDelegate__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "PDGameInstance.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_PatcherDemo_ChunkMountedDelegate__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_PatcherDemo, nullptr, "ChunkMountedDelegate__DelegateSignature", nullptr, nullptr, sizeof(_Script_PatcherDemo_eventChunkMountedDelegate_Parms), Z_Construct_UDelegateFunction_PatcherDemo_ChunkMountedDelegate__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_PatcherDemo_ChunkMountedDelegate__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_PatcherDemo_ChunkMountedDelegate__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_PatcherDemo_ChunkMountedDelegate__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_PatcherDemo_ChunkMountedDelegate__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_PatcherDemo_ChunkMountedDelegate__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UDelegateFunction_PatcherDemo_PatchCompleteDelegate__DelegateSignature_Statics
	{
		struct _Script_PatcherDemo_eventPatchCompleteDelegate_Parms
		{
			bool Succeeded;
		};
		static void NewProp_Succeeded_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_Succeeded;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UDelegateFunction_PatcherDemo_PatchCompleteDelegate__DelegateSignature_Statics::NewProp_Succeeded_SetBit(void* Obj)
	{
		((_Script_PatcherDemo_eventPatchCompleteDelegate_Parms*)Obj)->Succeeded = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UDelegateFunction_PatcherDemo_PatchCompleteDelegate__DelegateSignature_Statics::NewProp_Succeeded = { "Succeeded", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(_Script_PatcherDemo_eventPatchCompleteDelegate_Parms), &Z_Construct_UDelegateFunction_PatcherDemo_PatchCompleteDelegate__DelegateSignature_Statics::NewProp_Succeeded_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_PatcherDemo_PatchCompleteDelegate__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_PatcherDemo_PatchCompleteDelegate__DelegateSignature_Statics::NewProp_Succeeded,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_PatcherDemo_PatchCompleteDelegate__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "Comment", "// patching delegates\n" },
		{ "ModuleRelativePath", "PDGameInstance.h" },
		{ "ToolTip", "patching delegates" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_PatcherDemo_PatchCompleteDelegate__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_PatcherDemo, nullptr, "PatchCompleteDelegate__DelegateSignature", nullptr, nullptr, sizeof(_Script_PatcherDemo_eventPatchCompleteDelegate_Parms), Z_Construct_UDelegateFunction_PatcherDemo_PatchCompleteDelegate__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_PatcherDemo_PatchCompleteDelegate__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_PatcherDemo_PatchCompleteDelegate__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_PatcherDemo_PatchCompleteDelegate__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_PatcherDemo_PatchCompleteDelegate__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_PatcherDemo_PatchCompleteDelegate__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
class UScriptStruct* FPPatchStats::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern PATCHERDEMO_API uint32 Get_Z_Construct_UScriptStruct_FPPatchStats_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FPPatchStats, Z_Construct_UPackage__Script_PatcherDemo(), TEXT("PPatchStats"), sizeof(FPPatchStats), Get_Z_Construct_UScriptStruct_FPPatchStats_Hash());
	}
	return Singleton;
}
template<> PATCHERDEMO_API UScriptStruct* StaticStruct<FPPatchStats>()
{
	return FPPatchStats::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FPPatchStats(FPPatchStats::StaticStruct, TEXT("/Script/PatcherDemo"), TEXT("PPatchStats"), false, nullptr, nullptr);
static struct FScriptStruct_PatcherDemo_StaticRegisterNativesFPPatchStats
{
	FScriptStruct_PatcherDemo_StaticRegisterNativesFPPatchStats()
	{
		UScriptStruct::DeferCppStructOps(FName(TEXT("PPatchStats")),new UScriptStruct::TCppStructOps<FPPatchStats>);
	}
} ScriptStruct_PatcherDemo_StaticRegisterNativesFPPatchStats;
	struct Z_Construct_UScriptStruct_FPPatchStats_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FilesDownloaded_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_FilesDownloaded;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TotalFilesToDownload_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_TotalFilesToDownload;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DownloadPercent_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_DownloadPercent;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MBDownloaded_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_MBDownloaded;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TotalMBToDownload_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_TotalMBToDownload;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LastError_MetaData[];
#endif
		static const UE4CodeGen_Private::FTextPropertyParams NewProp_LastError;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FPPatchStats_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "// reports game patching stats that can be used for progress feedback\n" },
		{ "ModuleRelativePath", "PDGameInstance.h" },
		{ "ToolTip", "reports game patching stats that can be used for progress feedback" },
	};
#endif
	void* Z_Construct_UScriptStruct_FPPatchStats_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FPPatchStats>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FPPatchStats_Statics::NewProp_FilesDownloaded_MetaData[] = {
		{ "Category", "PPatchStats" },
		{ "ModuleRelativePath", "PDGameInstance.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FPPatchStats_Statics::NewProp_FilesDownloaded = { "FilesDownloaded", nullptr, (EPropertyFlags)0x0010000000000014, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FPPatchStats, FilesDownloaded), METADATA_PARAMS(Z_Construct_UScriptStruct_FPPatchStats_Statics::NewProp_FilesDownloaded_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FPPatchStats_Statics::NewProp_FilesDownloaded_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FPPatchStats_Statics::NewProp_TotalFilesToDownload_MetaData[] = {
		{ "Category", "PPatchStats" },
		{ "ModuleRelativePath", "PDGameInstance.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FPPatchStats_Statics::NewProp_TotalFilesToDownload = { "TotalFilesToDownload", nullptr, (EPropertyFlags)0x0010000000000014, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FPPatchStats, TotalFilesToDownload), METADATA_PARAMS(Z_Construct_UScriptStruct_FPPatchStats_Statics::NewProp_TotalFilesToDownload_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FPPatchStats_Statics::NewProp_TotalFilesToDownload_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FPPatchStats_Statics::NewProp_DownloadPercent_MetaData[] = {
		{ "Category", "PPatchStats" },
		{ "ModuleRelativePath", "PDGameInstance.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FPPatchStats_Statics::NewProp_DownloadPercent = { "DownloadPercent", nullptr, (EPropertyFlags)0x0010000000000014, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FPPatchStats, DownloadPercent), METADATA_PARAMS(Z_Construct_UScriptStruct_FPPatchStats_Statics::NewProp_DownloadPercent_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FPPatchStats_Statics::NewProp_DownloadPercent_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FPPatchStats_Statics::NewProp_MBDownloaded_MetaData[] = {
		{ "Category", "PPatchStats" },
		{ "ModuleRelativePath", "PDGameInstance.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FPPatchStats_Statics::NewProp_MBDownloaded = { "MBDownloaded", nullptr, (EPropertyFlags)0x0010000000000014, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FPPatchStats, MBDownloaded), METADATA_PARAMS(Z_Construct_UScriptStruct_FPPatchStats_Statics::NewProp_MBDownloaded_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FPPatchStats_Statics::NewProp_MBDownloaded_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FPPatchStats_Statics::NewProp_TotalMBToDownload_MetaData[] = {
		{ "Category", "PPatchStats" },
		{ "ModuleRelativePath", "PDGameInstance.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FPPatchStats_Statics::NewProp_TotalMBToDownload = { "TotalMBToDownload", nullptr, (EPropertyFlags)0x0010000000000014, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FPPatchStats, TotalMBToDownload), METADATA_PARAMS(Z_Construct_UScriptStruct_FPPatchStats_Statics::NewProp_TotalMBToDownload_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FPPatchStats_Statics::NewProp_TotalMBToDownload_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FPPatchStats_Statics::NewProp_LastError_MetaData[] = {
		{ "Category", "PPatchStats" },
		{ "ModuleRelativePath", "PDGameInstance.h" },
	};
#endif
	const UE4CodeGen_Private::FTextPropertyParams Z_Construct_UScriptStruct_FPPatchStats_Statics::NewProp_LastError = { "LastError", nullptr, (EPropertyFlags)0x0010000000000014, UE4CodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FPPatchStats, LastError), METADATA_PARAMS(Z_Construct_UScriptStruct_FPPatchStats_Statics::NewProp_LastError_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FPPatchStats_Statics::NewProp_LastError_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FPPatchStats_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FPPatchStats_Statics::NewProp_FilesDownloaded,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FPPatchStats_Statics::NewProp_TotalFilesToDownload,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FPPatchStats_Statics::NewProp_DownloadPercent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FPPatchStats_Statics::NewProp_MBDownloaded,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FPPatchStats_Statics::NewProp_TotalMBToDownload,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FPPatchStats_Statics::NewProp_LastError,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FPPatchStats_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_PatcherDemo,
		nullptr,
		&NewStructOps,
		"PPatchStats",
		sizeof(FPPatchStats),
		alignof(FPPatchStats),
		Z_Construct_UScriptStruct_FPPatchStats_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FPPatchStats_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FPPatchStats_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FPPatchStats_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FPPatchStats()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FPPatchStats_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_PatcherDemo();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("PPatchStats"), sizeof(FPPatchStats), Get_Z_Construct_UScriptStruct_FPPatchStats_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FPPatchStats_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FPPatchStats_Hash() { return 2449130328U; }
	DEFINE_FUNCTION(UPDGameInstance::execDownloadSingleChunk)
	{
		P_GET_PROPERTY(FIntProperty,Z_Param_ChunkID);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->DownloadSingleChunk(Z_Param_ChunkID);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UPDGameInstance::execIsDownloadingSingleChunks)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->IsDownloadingSingleChunks();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UPDGameInstance::execIsChunkLoaded)
	{
		P_GET_PROPERTY(FIntProperty,Z_Param_ChunkID);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->IsChunkLoaded(Z_Param_ChunkID);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UPDGameInstance::execGetPatchStatus)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FPPatchStats*)Z_Param__Result=P_THIS->GetPatchStatus();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UPDGameInstance::execPatchGame)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->PatchGame();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UPDGameInstance::execInitPatching)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_VariantName);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->InitPatching(Z_Param_VariantName);
		P_NATIVE_END;
	}
	void UPDGameInstance::StaticRegisterNativesUPDGameInstance()
	{
		UClass* Class = UPDGameInstance::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "DownloadSingleChunk", &UPDGameInstance::execDownloadSingleChunk },
			{ "GetPatchStatus", &UPDGameInstance::execGetPatchStatus },
			{ "InitPatching", &UPDGameInstance::execInitPatching },
			{ "IsChunkLoaded", &UPDGameInstance::execIsChunkLoaded },
			{ "IsDownloadingSingleChunks", &UPDGameInstance::execIsDownloadingSingleChunks },
			{ "PatchGame", &UPDGameInstance::execPatchGame },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UPDGameInstance_DownloadSingleChunk_Statics
	{
		struct PDGameInstance_eventDownloadSingleChunk_Parms
		{
			int32 ChunkID;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ChunkID;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UPDGameInstance_DownloadSingleChunk_Statics::NewProp_ChunkID = { "ChunkID", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PDGameInstance_eventDownloadSingleChunk_Parms, ChunkID), METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UPDGameInstance_DownloadSingleChunk_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((PDGameInstance_eventDownloadSingleChunk_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UPDGameInstance_DownloadSingleChunk_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(PDGameInstance_eventDownloadSingleChunk_Parms), &Z_Construct_UFunction_UPDGameInstance_DownloadSingleChunk_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UPDGameInstance_DownloadSingleChunk_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPDGameInstance_DownloadSingleChunk_Statics::NewProp_ChunkID,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPDGameInstance_DownloadSingleChunk_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPDGameInstance_DownloadSingleChunk_Statics::Function_MetaDataParams[] = {
		{ "Category", "Patching" },
		{ "Comment", "/** Adds a single chunk to the download list and starts the load and mount process */" },
		{ "ModuleRelativePath", "PDGameInstance.h" },
		{ "ToolTip", "Adds a single chunk to the download list and starts the load and mount process" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UPDGameInstance_DownloadSingleChunk_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPDGameInstance, nullptr, "DownloadSingleChunk", nullptr, nullptr, sizeof(PDGameInstance_eventDownloadSingleChunk_Parms), Z_Construct_UFunction_UPDGameInstance_DownloadSingleChunk_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UPDGameInstance_DownloadSingleChunk_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPDGameInstance_DownloadSingleChunk_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UPDGameInstance_DownloadSingleChunk_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPDGameInstance_DownloadSingleChunk()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UPDGameInstance_DownloadSingleChunk_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UPDGameInstance_GetPatchStatus_Statics
	{
		struct PDGameInstance_eventGetPatchStatus_Parms
		{
			FPPatchStats ReturnValue;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UPDGameInstance_GetPatchStatus_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PDGameInstance_eventGetPatchStatus_Parms, ReturnValue), Z_Construct_UScriptStruct_FPPatchStats, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UPDGameInstance_GetPatchStatus_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPDGameInstance_GetPatchStatus_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPDGameInstance_GetPatchStatus_Statics::Function_MetaDataParams[] = {
		{ "Category", "Patching" },
		{ "Comment", "/** Returns a patching status report we can use to populate progress bars, etc. */" },
		{ "ModuleRelativePath", "PDGameInstance.h" },
		{ "ToolTip", "Returns a patching status report we can use to populate progress bars, etc." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UPDGameInstance_GetPatchStatus_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPDGameInstance, nullptr, "GetPatchStatus", nullptr, nullptr, sizeof(PDGameInstance_eventGetPatchStatus_Parms), Z_Construct_UFunction_UPDGameInstance_GetPatchStatus_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UPDGameInstance_GetPatchStatus_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPDGameInstance_GetPatchStatus_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UPDGameInstance_GetPatchStatus_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPDGameInstance_GetPatchStatus()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UPDGameInstance_GetPatchStatus_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UPDGameInstance_InitPatching_Statics
	{
		struct PDGameInstance_eventInitPatching_Parms
		{
			FString VariantName;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_VariantName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_VariantName;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPDGameInstance_InitPatching_Statics::NewProp_VariantName_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UPDGameInstance_InitPatching_Statics::NewProp_VariantName = { "VariantName", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PDGameInstance_eventInitPatching_Parms, VariantName), METADATA_PARAMS(Z_Construct_UFunction_UPDGameInstance_InitPatching_Statics::NewProp_VariantName_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UPDGameInstance_InitPatching_Statics::NewProp_VariantName_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UPDGameInstance_InitPatching_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPDGameInstance_InitPatching_Statics::NewProp_VariantName,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPDGameInstance_InitPatching_Statics::Function_MetaDataParams[] = {
		{ "Category", "Patching" },
		{ "Comment", "/** Initializes the patching system with the passed deployment name */" },
		{ "ModuleRelativePath", "PDGameInstance.h" },
		{ "ToolTip", "Initializes the patching system with the passed deployment name" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UPDGameInstance_InitPatching_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPDGameInstance, nullptr, "InitPatching", nullptr, nullptr, sizeof(PDGameInstance_eventInitPatching_Parms), Z_Construct_UFunction_UPDGameInstance_InitPatching_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UPDGameInstance_InitPatching_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPDGameInstance_InitPatching_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UPDGameInstance_InitPatching_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPDGameInstance_InitPatching()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UPDGameInstance_InitPatching_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UPDGameInstance_IsChunkLoaded_Statics
	{
		struct PDGameInstance_eventIsChunkLoaded_Parms
		{
			int32 ChunkID;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ChunkID;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UPDGameInstance_IsChunkLoaded_Statics::NewProp_ChunkID = { "ChunkID", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PDGameInstance_eventIsChunkLoaded_Parms, ChunkID), METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UPDGameInstance_IsChunkLoaded_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((PDGameInstance_eventIsChunkLoaded_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UPDGameInstance_IsChunkLoaded_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(PDGameInstance_eventIsChunkLoaded_Parms), &Z_Construct_UFunction_UPDGameInstance_IsChunkLoaded_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UPDGameInstance_IsChunkLoaded_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPDGameInstance_IsChunkLoaded_Statics::NewProp_ChunkID,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPDGameInstance_IsChunkLoaded_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPDGameInstance_IsChunkLoaded_Statics::Function_MetaDataParams[] = {
		{ "Category", "Patching" },
		{ "Comment", "/** Returns true if a given chunk is downloaded and mounted */" },
		{ "ModuleRelativePath", "PDGameInstance.h" },
		{ "ToolTip", "Returns true if a given chunk is downloaded and mounted" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UPDGameInstance_IsChunkLoaded_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPDGameInstance, nullptr, "IsChunkLoaded", nullptr, nullptr, sizeof(PDGameInstance_eventIsChunkLoaded_Parms), Z_Construct_UFunction_UPDGameInstance_IsChunkLoaded_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UPDGameInstance_IsChunkLoaded_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPDGameInstance_IsChunkLoaded_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UPDGameInstance_IsChunkLoaded_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPDGameInstance_IsChunkLoaded()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UPDGameInstance_IsChunkLoaded_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UPDGameInstance_IsDownloadingSingleChunks_Statics
	{
		struct PDGameInstance_eventIsDownloadingSingleChunks_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UPDGameInstance_IsDownloadingSingleChunks_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((PDGameInstance_eventIsDownloadingSingleChunks_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UPDGameInstance_IsDownloadingSingleChunks_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(PDGameInstance_eventIsDownloadingSingleChunks_Parms), &Z_Construct_UFunction_UPDGameInstance_IsDownloadingSingleChunks_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UPDGameInstance_IsDownloadingSingleChunks_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPDGameInstance_IsDownloadingSingleChunks_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPDGameInstance_IsDownloadingSingleChunks_Statics::Function_MetaDataParams[] = {
		{ "Category", "Patching" },
		{ "Comment", "/** Returns whether the system is currently downloading single chunks */" },
		{ "ModuleRelativePath", "PDGameInstance.h" },
		{ "ToolTip", "Returns whether the system is currently downloading single chunks" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UPDGameInstance_IsDownloadingSingleChunks_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPDGameInstance, nullptr, "IsDownloadingSingleChunks", nullptr, nullptr, sizeof(PDGameInstance_eventIsDownloadingSingleChunks_Parms), Z_Construct_UFunction_UPDGameInstance_IsDownloadingSingleChunks_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UPDGameInstance_IsDownloadingSingleChunks_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPDGameInstance_IsDownloadingSingleChunks_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UPDGameInstance_IsDownloadingSingleChunks_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPDGameInstance_IsDownloadingSingleChunks()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UPDGameInstance_IsDownloadingSingleChunks_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UPDGameInstance_PatchGame_Statics
	{
		struct PDGameInstance_eventPatchGame_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UPDGameInstance_PatchGame_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((PDGameInstance_eventPatchGame_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UPDGameInstance_PatchGame_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(PDGameInstance_eventPatchGame_Parms), &Z_Construct_UFunction_UPDGameInstance_PatchGame_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UPDGameInstance_PatchGame_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPDGameInstance_PatchGame_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPDGameInstance_PatchGame_Statics::Function_MetaDataParams[] = {
		{ "Category", "Patching" },
		{ "Comment", "/** Starts the game patching process. Returns false if the patching manifest is not up to date. */" },
		{ "ModuleRelativePath", "PDGameInstance.h" },
		{ "ToolTip", "Starts the game patching process. Returns false if the patching manifest is not up to date." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UPDGameInstance_PatchGame_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPDGameInstance, nullptr, "PatchGame", nullptr, nullptr, sizeof(PDGameInstance_eventPatchGame_Parms), Z_Construct_UFunction_UPDGameInstance_PatchGame_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UPDGameInstance_PatchGame_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPDGameInstance_PatchGame_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UPDGameInstance_PatchGame_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPDGameInstance_PatchGame()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UPDGameInstance_PatchGame_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UPDGameInstance_NoRegister()
	{
		return UPDGameInstance::StaticClass();
	}
	struct Z_Construct_UClass_UPDGameInstance_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnPatchReady_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnPatchReady;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnPatchComplete_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnPatchComplete;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PatchVersionURL_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_PatchVersionURL;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ChunkDownloadList_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ChunkDownloadList_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ChunkDownloadList;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnSingleChunkPatchComplete_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnSingleChunkPatchComplete;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UPDGameInstance_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UGameInstance,
		(UObject* (*)())Z_Construct_UPackage__Script_PatcherDemo,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UPDGameInstance_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UPDGameInstance_DownloadSingleChunk, "DownloadSingleChunk" }, // 1634538661
		{ &Z_Construct_UFunction_UPDGameInstance_GetPatchStatus, "GetPatchStatus" }, // 1263475518
		{ &Z_Construct_UFunction_UPDGameInstance_InitPatching, "InitPatching" }, // 2854877458
		{ &Z_Construct_UFunction_UPDGameInstance_IsChunkLoaded, "IsChunkLoaded" }, // 965039654
		{ &Z_Construct_UFunction_UPDGameInstance_IsDownloadingSingleChunks, "IsDownloadingSingleChunks" }, // 2353732732
		{ &Z_Construct_UFunction_UPDGameInstance_PatchGame, "PatchGame" }, // 2418667227
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPDGameInstance_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n *  Extends GameInstance functionality to provide runtime chunk download and patching\n */" },
		{ "IncludePath", "PDGameInstance.h" },
		{ "ModuleRelativePath", "PDGameInstance.h" },
		{ "ToolTip", "Extends GameInstance functionality to provide runtime chunk download and patching" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPDGameInstance_Statics::NewProp_OnPatchReady_MetaData[] = {
		{ "Category", "Patching" },
		{ "Comment", "/** Fired when the patching manifest has been queried and we're ready to decide whether to patch the game or not*/" },
		{ "ModuleRelativePath", "PDGameInstance.h" },
		{ "ToolTip", "Fired when the patching manifest has been queried and we're ready to decide whether to patch the game or not" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UPDGameInstance_Statics::NewProp_OnPatchReady = { "OnPatchReady", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPDGameInstance, OnPatchReady), Z_Construct_UDelegateFunction_PatcherDemo_PatchCompleteDelegate__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UPDGameInstance_Statics::NewProp_OnPatchReady_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPDGameInstance_Statics::NewProp_OnPatchReady_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPDGameInstance_Statics::NewProp_OnPatchComplete_MetaData[] = {
		{ "Category", "Patching" },
		{ "Comment", "/** Fired when the patching process succeeds or fails */" },
		{ "ModuleRelativePath", "PDGameInstance.h" },
		{ "ToolTip", "Fired when the patching process succeeds or fails" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UPDGameInstance_Statics::NewProp_OnPatchComplete = { "OnPatchComplete", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPDGameInstance, OnPatchComplete), Z_Construct_UDelegateFunction_PatcherDemo_PatchCompleteDelegate__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UPDGameInstance_Statics::NewProp_OnPatchComplete_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPDGameInstance_Statics::NewProp_OnPatchComplete_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPDGameInstance_Statics::NewProp_PatchVersionURL_MetaData[] = {
		{ "Category", "Patching" },
		{ "Comment", "/** URL to query for patch version */" },
		{ "ModuleRelativePath", "PDGameInstance.h" },
		{ "ToolTip", "URL to query for patch version" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UPDGameInstance_Statics::NewProp_PatchVersionURL = { "PatchVersionURL", nullptr, (EPropertyFlags)0x0020080000010001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPDGameInstance, PatchVersionURL), METADATA_PARAMS(Z_Construct_UClass_UPDGameInstance_Statics::NewProp_PatchVersionURL_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPDGameInstance_Statics::NewProp_PatchVersionURL_MetaData)) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UPDGameInstance_Statics::NewProp_ChunkDownloadList_Inner = { "ChunkDownloadList", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPDGameInstance_Statics::NewProp_ChunkDownloadList_MetaData[] = {
		{ "Category", "Patching" },
		{ "Comment", "/** List of Chunk IDs to try and download */" },
		{ "ModuleRelativePath", "PDGameInstance.h" },
		{ "ToolTip", "List of Chunk IDs to try and download" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UPDGameInstance_Statics::NewProp_ChunkDownloadList = { "ChunkDownloadList", nullptr, (EPropertyFlags)0x0020080000010001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPDGameInstance, ChunkDownloadList), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UPDGameInstance_Statics::NewProp_ChunkDownloadList_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPDGameInstance_Statics::NewProp_ChunkDownloadList_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPDGameInstance_Statics::NewProp_OnSingleChunkPatchComplete_MetaData[] = {
		{ "Category", "Patching" },
		{ "Comment", "/** Fired when the patching process succeeds or fails */" },
		{ "ModuleRelativePath", "PDGameInstance.h" },
		{ "ToolTip", "Fired when the patching process succeeds or fails" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UPDGameInstance_Statics::NewProp_OnSingleChunkPatchComplete = { "OnSingleChunkPatchComplete", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPDGameInstance, OnSingleChunkPatchComplete), Z_Construct_UDelegateFunction_PatcherDemo_PatchCompleteDelegate__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UPDGameInstance_Statics::NewProp_OnSingleChunkPatchComplete_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPDGameInstance_Statics::NewProp_OnSingleChunkPatchComplete_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UPDGameInstance_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPDGameInstance_Statics::NewProp_OnPatchReady,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPDGameInstance_Statics::NewProp_OnPatchComplete,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPDGameInstance_Statics::NewProp_PatchVersionURL,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPDGameInstance_Statics::NewProp_ChunkDownloadList_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPDGameInstance_Statics::NewProp_ChunkDownloadList,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPDGameInstance_Statics::NewProp_OnSingleChunkPatchComplete,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UPDGameInstance_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UPDGameInstance>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UPDGameInstance_Statics::ClassParams = {
		&UPDGameInstance::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UPDGameInstance_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UPDGameInstance_Statics::PropPointers),
		0,
		0x009000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UPDGameInstance_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UPDGameInstance_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UPDGameInstance()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UPDGameInstance_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UPDGameInstance, 4036747915);
	template<> PATCHERDEMO_API UClass* StaticClass<UPDGameInstance>()
	{
		return UPDGameInstance::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UPDGameInstance(Z_Construct_UClass_UPDGameInstance, &UPDGameInstance::StaticClass, TEXT("/Script/PatcherDemo"), TEXT("UPDGameInstance"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UPDGameInstance);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
