// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "PatcherDemo/PatcherDemoGameModeBase.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePatcherDemoGameModeBase() {}
// Cross Module References
	PATCHERDEMO_API UClass* Z_Construct_UClass_APatcherDemoGameModeBase_NoRegister();
	PATCHERDEMO_API UClass* Z_Construct_UClass_APatcherDemoGameModeBase();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	UPackage* Z_Construct_UPackage__Script_PatcherDemo();
// End Cross Module References
	DEFINE_FUNCTION(APatcherDemoGameModeBase::execTryServerTravel)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_URL);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->TryServerTravel(Z_Param_URL);
		P_NATIVE_END;
	}
	void APatcherDemoGameModeBase::StaticRegisterNativesAPatcherDemoGameModeBase()
	{
		UClass* Class = APatcherDemoGameModeBase::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "TryServerTravel", &APatcherDemoGameModeBase::execTryServerTravel },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_APatcherDemoGameModeBase_TryServerTravel_Statics
	{
		struct PatcherDemoGameModeBase_eventTryServerTravel_Parms
		{
			FString URL;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_URL_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_URL;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APatcherDemoGameModeBase_TryServerTravel_Statics::NewProp_URL_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_APatcherDemoGameModeBase_TryServerTravel_Statics::NewProp_URL = { "URL", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PatcherDemoGameModeBase_eventTryServerTravel_Parms, URL), METADATA_PARAMS(Z_Construct_UFunction_APatcherDemoGameModeBase_TryServerTravel_Statics::NewProp_URL_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_APatcherDemoGameModeBase_TryServerTravel_Statics::NewProp_URL_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_APatcherDemoGameModeBase_TryServerTravel_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_APatcherDemoGameModeBase_TryServerTravel_Statics::NewProp_URL,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APatcherDemoGameModeBase_TryServerTravel_Statics::Function_MetaDataParams[] = {
		{ "Category", "DLC" },
		{ "ModuleRelativePath", "PatcherDemoGameModeBase.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_APatcherDemoGameModeBase_TryServerTravel_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_APatcherDemoGameModeBase, nullptr, "TryServerTravel", nullptr, nullptr, sizeof(PatcherDemoGameModeBase_eventTryServerTravel_Parms), Z_Construct_UFunction_APatcherDemoGameModeBase_TryServerTravel_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_APatcherDemoGameModeBase_TryServerTravel_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_APatcherDemoGameModeBase_TryServerTravel_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_APatcherDemoGameModeBase_TryServerTravel_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_APatcherDemoGameModeBase_TryServerTravel()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_APatcherDemoGameModeBase_TryServerTravel_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_APatcherDemoGameModeBase_NoRegister()
	{
		return APatcherDemoGameModeBase::StaticClass();
	}
	struct Z_Construct_UClass_APatcherDemoGameModeBase_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_APatcherDemoGameModeBase_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameModeBase,
		(UObject* (*)())Z_Construct_UPackage__Script_PatcherDemo,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_APatcherDemoGameModeBase_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_APatcherDemoGameModeBase_TryServerTravel, "TryServerTravel" }, // 409141806
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APatcherDemoGameModeBase_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "PatcherDemoGameModeBase.h" },
		{ "ModuleRelativePath", "PatcherDemoGameModeBase.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_APatcherDemoGameModeBase_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<APatcherDemoGameModeBase>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_APatcherDemoGameModeBase_Statics::ClassParams = {
		&APatcherDemoGameModeBase::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x009002ACu,
		METADATA_PARAMS(Z_Construct_UClass_APatcherDemoGameModeBase_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_APatcherDemoGameModeBase_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_APatcherDemoGameModeBase()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_APatcherDemoGameModeBase_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(APatcherDemoGameModeBase, 3969590435);
	template<> PATCHERDEMO_API UClass* StaticClass<APatcherDemoGameModeBase>()
	{
		return APatcherDemoGameModeBase::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_APatcherDemoGameModeBase(Z_Construct_UClass_APatcherDemoGameModeBase, &APatcherDemoGameModeBase::StaticClass, TEXT("/Script/PatcherDemo"), TEXT("APatcherDemoGameModeBase"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(APatcherDemoGameModeBase);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
